package rules.c1_2_white_space;

import java.util.ArrayList;
import java.util.Iterator;

public class A {
	static Character.UnicodeBlock block, last = null;

	public static void main(String[] args) {
		ArrayList space = new ArrayList(), whitespace = new ArrayList(), both = new ArrayList();
		for (int i = 0; i < Character.MAX_VALUE; i++) {
			char ch = (char) i;
			Character wrapper = new Character(ch);
			if (Character.isWhitespace(ch)) {
				if (Character.isSpaceChar(ch)) {
					both.add(wrapper);
				} else {
					whitespace.add(wrapper);
				}
			} else if (Character.isSpaceChar(ch)) {
				space.add(wrapper);
			}
		}
		if (false) {
			/* 用if(false)注释代码 */
			System.out.println("用if(false)注释代码");
		}
		System.out.println("Hello\u00A0World!");
		System.out.println();
		System.out.println("isSpaceChar() ONLY...");
		for (Iterator i = space.iterator(); i.hasNext();) {
			print(((Character) i.next()).charValue());
		}
		last = null;
		System.out.println();
		System.out.println();
		System.out.println("isWhitespace() ONLY...");
		for (Iterator i = whitespace.iterator(); i.hasNext();) {
			print(((Character) i.next()).charValue());
		}
		last = null;
		System.out.println();
		System.out.println();
		System.out.println("BOTH...");
		for (Iterator i = both.iterator(); i.hasNext();) {
			print(((Character) i.next()).charValue());
		}
	}

	static void print(char ch) {
		block = Character.UnicodeBlock.of(ch);
		if (!block.equals(last)) {
			if (last != null) {
				System.out.println();
			}
			System.out.println(block.toString());
			last = block;
		}
		System.out.print(toUnicodeEscape(ch) + " ");
	}

	static String toUnicodeEscape(char ch) {
		String s = "" + Character.forDigit(((ch >>> 12) & 0xF), 16) //
				+ Character.forDigit(((ch >>> 8) & 0xF), 16) //
				+ Character.forDigit(((ch >>> 4) & 0xF), 16) //
				+ Character.forDigit((ch) & 0xF, 16);
		return "\\u" + s.toUpperCase();
	}
}
