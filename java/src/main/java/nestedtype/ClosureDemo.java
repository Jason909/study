package nestedtype;

import java.util.ArrayList;
import java.util.List;

public class ClosureDemo {
	int a = 1;
	static int b = 1;

	public static void main(String[] args) {
		List<Listener> list = staticFoo();
		for (Listener listener : list) {
			System.out.println(listener.handle());
		}
		list = new ClosureDemo().foo();
		for (Listener listener : list) {
			System.out.println(listener.handle());
		}
	}

	interface Listener {
		public int handle();
	}

	static List<Listener> staticFoo() {
		new Listener() {
			public int handle() {
				// No enclosing instance of the type ClosureDemo is accessible in scope
				System.out.println(ClosureDemo.this.a);
				System.out.println(ClosureDemo.b);
				return 0;
			}
		};

		// The member interface LocalInterface can only be defined inside a top-level
		// class or interface or in a static context
		interface LocalInterface{}

		class LocalClass {
			// The field a cannot be declared static in a non-static inner type, unless
			// initialized with a constant expression
			static int a = 1;

			// The method f cannot be declared static; static methods can only be declared
			// in a static or top level type
			static void f() {
			};
		}
		System.out.println(LocalClass.a);

		class Closure {
			int i;

			void foo() {
				// No enclosing instance of the type ClosureDemo is accessible in scope
				System.out.println(ClosureDemo.this.a);
				System.out.println(ClosureDemo.b);
			}
		}
		final Closure closure = new Closure();
		closure.foo();

		System.out.println(closure.i);
		closure.i = 1;
		System.out.println(closure.i);
		List<Listener> list = new ArrayList<Listener>();
		list.add(new Listener() {
			public int handle() {
				return closure.i += 100;
			}
		});
		list.add(new Listener() {
			public int handle() {
				return closure.i += 1000;
			}
		});
		closure.i += 10;
		System.out.println(closure.i);
		return list;
	}

	class FooClosureNG {
		// The field a cannot be declared static in a non-static inner type, unless
		// initialized with a constant expression
		static int a = 1;
		int i;

		// The method f cannot be declared static; static methods can only be declared
		// in a static or top level type
		static void f() {
		};

		void foo() {
			System.out.println(ClosureDemo.this.a);
			System.out.println(ClosureDemo.b);
		}
	}

	static class FooClosure {
		static int a = 1;
		int i;

		static void f() {
		};

		interface NestedInterface {
		}

		void foo() {
			// No enclosing instance of the type ClosureDemo is accessible in scope
			System.out.println(ClosureDemo.this.a);
			System.out.println(ClosureDemo.b);
		}

		Listener foo1(final int i) {
			final FooClosure closure = this;
			return new Listener() {
				public int handle() {
					return closure.i += i;
				}
			};
		}
	}

	List<Listener> foo() {
		new Listener() {
			public int handle() {
				System.out.println(ClosureDemo.this.a);
				System.out.println(ClosureDemo.b);
				return 0;
			}
		};

		class Closure {
			// The field a cannot be declared static in a non-static inner type, unless
			// initialized with a constant expression
			static int a = 1;
			int i;

			// The method f cannot be declared static; static methods can only be declared
			// in a static or top level type
			static void f() {
			};

			void foo() {
				System.out.println(ClosureDemo.this.a);
				System.out.println(ClosureDemo.b);
			}
		}
		final Closure closureNG1 = new Closure();
		System.out.println(closureNG1.i);
		closureNG1.foo();

		final FooClosureNG closureNG2 = new FooClosureNG();
		closureNG2.foo();

		final FooClosure closure = new FooClosure();
		closure.foo();

		System.out.println(closure.i);
		closure.i = 1;
		System.out.println(closure.i);
		List<Listener> list = new ArrayList<Listener>();
		list.add(closure.foo1(100));
		list.add(closure.foo1(1000));
		closure.i += 10;
		System.out.println(closure.i);
		return list;
	}

}
