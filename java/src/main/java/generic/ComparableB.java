package generic;

//The type ComparableB cannot extend or implement Comparable<? super ComparableB>. A supertype may not specify any wildcard
public class ComparableB implements Comparable<? super ComparableB> {
}
