package generic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class ComparableC extends C implements Comparable<B> {
	public int compareTo(B o) {
		return index.compareTo(o.index);
	}

	public void testMax() {
		Collection<ComparableC> colC = new ArrayList<ComparableC>();
		Collections.max(colC);
		// Bound mismatch: The generic method max(Collection<T>) of type ComparableA is
		// not applicable for the arguments (Collection<ComparableC>). The inferred type
		// ComparableC is not a valid substitute for the bounded parameter <T extends
		// Object & Comparable<T>>
		ComparableA.max(colC);

		Collection<ComparableD> colD = new ArrayList<ComparableD>();
		colD.add(new ComparableD());
		Collection<? extends ComparableC> col = colD;
		Collections.max(col);
	}
}
