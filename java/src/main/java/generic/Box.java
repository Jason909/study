package generic;

public class Box<T> {
	private T element;

	public T get() {
		return element;
	}

	public void put(T element) {
		this.element = element;
	}

	public void rebox() {
		put(get());
	}

	public static <V> Box<V> make() {
		return new Box<V>();
	}

	public static void main(String[] args) {
		Box box = Box.make();// 编译警告 Box is a raw type. References to generic type Box<T> should be parameterized
		Box<A> ba = Box.make();
		Box<B> bb = Box.make();
		Box<C> bc = Box.make();
		bb = box;// 编译警告 Type safety: The expression of type Box needs unchecked conversion to conform to Box<B>

		Box<? extends B> bbOut = bc;
		Box<? extends A> baOut = bbOut;
		bbOut = ba;// 编译错误 Type mismatch: cannot convert from Box<A> to Box<? extends B>

		Box<? super B> bbIn = ba;
		Box<? super C> bcIn = bbIn;
		bbIn = bc;// 编译错误 Type mismatch: cannot convert from Box<C> to Box<? super B>

		Box<? extends Object> boOut = bcIn;
		Box<?> x = boOut;
		boOut = x;

		Object o = new Object();
		A a = new A();
		B b = new B();
		C c = new C();

		o = x.get();// 返回Object
		a = x.get();// 编译错误 Type mismatch: cannot convert from capture#10-of ? to A
		x.put(null);
		x.put(c);// 编译错误 The method put(capture#12-of ?) in the type Box<capture#12-of ?> is not applicable for the arguments (C)

		o = boOut.get();// 返回Object
		a = boOut.get();// 编译错误 Type mismatch: cannot convert from capture#14-of ? extends Object to A
		boOut.put(null);
		boOut.put(c);// 编译错误 The method put(capture#16-of ? extends Object) in the type Box<capture#16-of ? extends Object> is not applicable for the arguments (C)

		a = bbOut.get();// 返回B
		c = bbOut.get();// 编译错误 Type mismatch: cannot convert from capture#18-of ? extends B to C
		bbOut.put(null);
		bbOut.put(c);// 编译错误 The method put(capture#20-of ? extends B) in the type Box<capture#20-of ? extends B> is not applicable for the arguments (C)

		o = bbIn.get();// 返回Object
		a = bbIn.get();// 编译错误 Type mismatch: cannot convert from capture#22-of ? super B to A
		bbIn.put(c);
		bbIn.put(a);// 编译错误 The method put(capture#24-of ? super B) in the type Box<capture#24-of ? super B> is not applicable for the arguments (A)

		x.rebox();
	}
}
