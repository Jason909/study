package generic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ComparableA implements Comparable<ComparableA> {
	Integer index;

	public int compareTo(ComparableA o) {
		return index.compareTo(o.index);
	}

	public static <T extends Object & Comparable<T>> T max(Collection<T> coll) {
		Iterator<? extends T> i = coll.iterator();
		T candidate = i.next();

		while (i.hasNext()) {
			T next = i.next();
			if (next.compareTo(candidate) > 0)
				candidate = next;
		}
		return candidate;
	}

	public static void testMax() {
		Collection<ComparableA> col = new ArrayList<ComparableA>();
		max(col);
	}

}
